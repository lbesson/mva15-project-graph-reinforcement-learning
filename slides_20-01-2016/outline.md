# Outline for our slides : RL/GML
- Time : **max** 15 minutes
- Real time : **max** 13 minutes
- Slides : 10 - 13 slides **max**
- When ? Tuesday  19 January (salle Condorcet) at 11:15, and Wednesday 20 January at 11: (over Skype)
- With who ? Emilie K. 19-01, Christos D. 20-01
- By who? <http://lbo.k.vu/gml2016>

## First draft of outline
1. Introduction (1)
2. Presentation (2-3)
3. Single-expert setting (2), Toussou13
4. Generalizing to multi-expert setting (2), Intuitive idea \& limitations
5. Learning on the opponents (2), Combining [Pengkun15] and [Toussou13]
6. ? Experiments, successes and failures (2)
7. Conclusion (2)
8. Appendix (1-2)

## Overview, slide by slides
1. Introduction 1 : Title, team, supervisor, courses, date (**OK**)
2. Presentation 1 : Problem we considered : learn to play a board game. Hypothesis : full-knowledge, two players, turn based, finite state space and action space,
3. Presentation 2 : Naive approach : complete tree search (minimax). Fails if the tree is too big
4. Presentation 3 : Idea = learn from trajectories or demonstrations (from a database, or generated ones). A few notations to introduce here
5. Single-expert 1 : Learn $w\_m$ by LSTD-Q on the demonstrations $d\_m$, several trajectories but one expert
6. Single-expert 2 : Result our LSTD-Q implementation works perfectly well (confirming [Toussou13])
7. Single-expert 3 : A table of result ?
8. Multi-expert 1 : Main idea, combine the $w\_m$ linearly with a prior. Example
9. Multi-expert 2 : Problem with this prior, trying to fix it (2 ideas : temperature [failed], testing against $pi\_0$ [~ work])
10. Learning on the opponents 1 : LSTD-Q on the opponents $m = 1 \dots M$, then Pengkun on players $m = 1 \dots M$, works well !
11. Learning on the opponents 2 : Table of results, summing up the experiments
12. Conclusion 1 : Technical conclusion, a short sum-up of what we did and presented, of what we tried, and what worked. If possible, evoke orally the limitations
13. Conclusion 2 : Thanks, "Any questions ?", our emails, and a short list of references (Pengkun15, Toussou13, Rothkopf12) (**OK**)
14. Appendix 1, 2, 3 max : log files output of experiments / tables of performance / figures of other experiments

