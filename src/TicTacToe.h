// (C) Basile Clement and Lilian Besson, 2015
// https://bitbucket.org/lbesson/mva15-project-graph-reinforcement-learning
#include <array>
#include <vector>
#include <cstddef>
#include <stdexcept>
#include <cassert>


// beliefbox (from https://github.com/olethrosdc/beliefbox)
#include "../beliefbox/src/core/Vector.h"

enum class Player { Blank, White, Black };

// We want this to be inlined for speed
class Board {
public:
  class Reference {
  private:
    friend class Board;

    Reference(std::array<std::vector<bool>::reference, 2> refs)
      : m_refs(refs)
    {
    }

  public:
    operator Player() const
    {
      if (!m_refs[1])
	return Player::Blank;

      return m_refs[0] ? Player::White : Player::Black;
    }

    Reference& operator=(const Player p)
    {
      if (p == Player::Blank)
	m_refs[0] = m_refs[1] = false;
      else {
	m_refs[1] = true;
	m_refs[0] = p == Player::White;
      }
      return *this;
    }

    Reference& operator=(const Reference& r) { return *this = Player(r); }

  private:
    std::array<std::vector<bool>::reference, 2> m_refs;
  };

  Board(size_t size = 3)
    : m_size(size)
    , m_bitset(4 * size * size)
  {
  }

  size_t Size() const { return m_size; }

  Player operator()(size_t i, size_t j) const
  {
    size_t pos = IndexOf(i, j);
    if (!m_bitset[pos + 1])
      return Player::Blank;

    return m_bitset[pos] ? Player::White : Player::Black;
  }

  Reference operator()(size_t i, size_t j)
  {
    size_t pos = IndexOf(i, j);
    return { { { m_bitset[pos], m_bitset[pos + 1] } } };
  }

  bool operator==(const Board& other) const
  {
    assert(other.m_size == m_size);
    return m_bitset == other.m_bitset;
  }

  size_t hash() const
  {
    return std::hash<std::vector<bool>>{}(m_bitset);
  }

private:
  size_t IndexOf(size_t i, size_t j) const
  {
    // We use two bits for each tile
    return i * 2 * m_size + 2 * j;
  }

  size_t m_size;
  // std::vector<bool> is usually implemented as a bitset
  std::vector<bool> m_bitset;
};

class TicTacToe {
public:
  TicTacToe(size_t size)
    : m_ended(false)
    , m_nextPlayer(Player::White)
    , m_board(size)
  {
  }

  Player NextPlayer() const { return m_nextPlayer; }

  Player Winner() const { return m_ended ? m_nextPlayer : Player::Blank; }

  size_t Size() const { return m_board.Size(); }

  Player operator()(size_t i, size_t j) const { return m_board(i, j); }

  void Play(size_t i, size_t j);

  TicTacToe WithPlay(size_t i, size_t j) const
  {
    TicTacToe game(*this);
    game.Play(i, j);
    return game;
  }

  TicTacToe WithPlay(std::pair<size_t, size_t> a) const { return WithPlay(a.first, a.second); }

  template<typename Functor>
  void ForEachMove(const Functor& f) const
  {
    if (m_ended)
      return;

    size_t ix = 0;
    for (size_t i = 0; i < Size(); ++i) {
      for (size_t j = 0; j < Size(); ++j) {
	if (m_board(i, j) == Player::Blank)
	  f(ix++, WithPlay(i, j));
      }
    }
  }

  size_t NumberOfMoves() const
  {
    if (m_ended)
      return 0;

    size_t ix = 0;
    for (size_t i = 0; i < Size(); ++i) {
      for (size_t j = 0; j < Size(); ++j) {
	if (m_board(i, j) == Player::Blank)
	  ++ix;
      }
    }
    return ix;
  }

  std::pair<size_t, size_t> MakeMove(size_t ix)
  {
    if (!m_ended) {
      for (size_t i = 0; i < Size(); ++i) {
	for (size_t j = 0; j < Size(); ++j) {
	  if (m_board(i, j) == Player::Blank && !ix--) {
	    Play(i, j);
	    return { i, j };
	  }
	}
      }
    }
    throw std::runtime_error("move index is too high");
  }

  TicTacToe WithMove(size_t ix) const
  {
    TicTacToe next(*this);
    next.MakeMove(ix);
    return next;
  }

  bool operator==(const TicTacToe& other) const
  {
    assert(other.Size() == Size());
    if (other.m_board == m_board) {
      assert(other.m_ended == m_ended);
      assert(other.m_nextPlayer == m_nextPlayer);
      return true;
    }
    return false;
  }

  size_t hash() const
  {
    return m_board.hash();
  }

  bool Ended() const { return NumberOfMoves() == 0; }

  Vector features() const;

  void print(FILE* f) const;

private:
  bool m_ended;
  Player m_nextPlayer;
  Board m_board;
};

namespace std
{
  template<>
  struct hash<Board>
  {
    typedef Board argument_type;
    typedef size_t result_type;
    result_type operator()(argument_type const& board) const
    {
      return board.hash();
    }
  };

  template<>
  struct hash<TicTacToe>
  {
    typedef TicTacToe argument_type;
    typedef size_t result_type;
    result_type operator()(argument_type const& game) const
    {
      return game.hash();
    }
  };
};
