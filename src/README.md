# Code for our [*Multi-task inference and planning in board games using multiple imperfect oracles* project](../)

## Dependencies...
### Usual libraries (dependencies)
> Our C++ code is using the following free and open-source libraries:

- [Boost MPL (Meta-Programing Library)](http://www.boost.org/doc/libs/1_60_0/libs/mpl/doc/index.html) (option ``-lmpl`` to c++/clang++),
- [GSL (Gnu Scientific Library)](https://www.gnu.org/software/gsl/) (option ``-lgsl`` to c++/clang++),
- [CBLAS (Basic Linear Algebra Subprograms)](http://www.netlib.org/blas/) (option ``-lcblas`` to c++/clang++),
- [ranlib (General Random Number Generators (RNG's) )](http://people.sc.fsu.edu/~jburkardt/cpp_src/ranlib/ranlib.html) (option ``-lranlib`` to c++/clang++).

All these can be installed from your usual package manager (with dev packages).

### Dependencies from GitHub
We are using:

- Christos Dimitrakakis's [beliefbox](https://github.com/olethrosdc/beliefbox) (available from GitHub) is a strict dependency,
- The thesis master code of Pengkun Liu : [Charles-Lau-/thesis/code](https://github.com/Charles-Lau-/thesis/tree/master/code) was an inspiration (available from GitHub).

----

## How to use ?
> Keep in mind this code is only for academic purpose, and is still very experimental.

### Cloning the git repository
```bash
git clone https://bitbucket.org/lbesson/mva15-project-graph-reinforcement-learning
cd mva15-project-graph-reinforcement-learning/
git submodule init
git submodule update
```

### Compiling beliefbox
- Go to ``beliefbox/`` folder, then follow their instruction to compile it.

### Compiling the code
This assumes you have built [beliefbox](https://github.com/olethrosdc/beliefbox) in ``beliefbox/``, and keep the same organization for the source.

```bash
cd src/
make
```

- Note that the [Makefile](./Makefile) might not be universal enough, it was tested on Mac OS X and Ubuntu Linux only.

### Running the code
After building and in the same folder:
```bash
./main
```

- Right now, it performs the last experiment explained in [our report](../report_final/).

---

## More details about the code
- First, read [our written report](../report_final/) for a better understanding of the notations, concepts and the entire framework.
- And then, read the code itself (there is a few comments when it is useful).
