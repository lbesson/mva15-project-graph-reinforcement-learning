# [*Graphs in Machine Learning*](http://researchers.lille.inria.fr/~valko/hp/mva-ml-graphs.php) and [*Reinforcement Learning*](http://researchers.lille.inria.fr/~lazaric/Webpage/MVA-RL_Course15.html) project (Master MVA)
## Meta-data:
- *Subject*: *Multi-task inference and planning in board games using multiple imperfect oracles*,
- *Advisor*: [Christos Dimitrakakis](http://www.cse.chalmers.se/~chrdimi/),
- *Keywords*: Modelling, Algorithms, Implementation,
- *Reference*: [on Prof. Valko's website](http://researchers.lille.inria.fr/~valko/hp/mvaprojects.php),
- *Where*: [git repository on bitbucket.org](https://bitbucket.org/lbesson/mva15-project-graph-reinforcement-learning),
- *Application*: machine learning, reinforcement learning, planning, Bayesian statistics.

## Description:
Sequential decision making problems in games such as chess can in theory be solved via the principle of backwards induction.
By assigning values to the outcomes of the game (win, loss, draw), we can calculate the value of any terminal state of the game.
Working our way backwards, we can calculate the expected value of any previous state, given sufficient resources.

The aim of the project is to develop a principled statistical approach for the integration of planning with expert play information from multiple experts.

This involved *developing* multitask inverse reinforcement learning models to take advantage of the available ground truth, and then *applying* it to the problem of learning from expert play.

---

## [Code](./src/)
See [this README](./src/README.md) file for more details on how to download, build and execute our code.

### Cloning the git repository
```bash
git clone https://bitbucket.org/lbesson/mva15-project-graph-reinforcement-learning
cd mva15-project-graph-reinforcement-learning/
git submodule init
git submodule update
```

----

## Main documents
### [Slides](./slides_20-01-2016) (19th of January)
- 15-20 slides for 15 minutes max, for the oral presentation on 19-01-2016.
- [See the slides](https://bitbucket.org/lbesson/mva15-project-graph-reinforcement-learning/downloads/MVA_GML_RL__project__Lilian_Besson__Basile_Clement__Slides.en.pdf).

### [Final report](./report/) (deadline 09th of January)
- 5-10 pages projet report, due to 09-01-2016.
- [See the report](https://bitbucket.org/lbesson/mva15-project-graph-reinforcement-learning/downloads/MVA_GML_RL__project__Lilian_Besson__Basile_Clement__Final_report.en.pdf).
- [Following the NIPS'15 styles](https://nips.cc/Conferences/2015/PaperInformation/StyleFiles).

----

## About
This project was done for the [Graphs in Machine Learning](http://researchers.lille.inria.fr/~valko/hp/mva-ml-graphs.php) and [Reinforcement Learning](http://researchers.lille.inria.fr/~lazaric/Webpage/MVA-RL_Course15.html) courses for the [MVA master program](http://www.cmla.ens-cachan.fr/version-anglaise/academics/mva-master-degree-227777.kjsp) at [ENS de Cachan](http://www.ens-cachan.fr).

### Copyright
(C), 2015-16, [Lilian Besson](http://perso.crans.org/besson/) ([ENS de Cachan](http://www.ens-cachan.fr)) and [Basile Clement](http://www.eleves.ens.fr/home/bclement/CV.pdf) ([ENS Ulm](http://www.ens.fr)).

### Licence
This project is publicly published, under the terms of the [MIT license](http://lbesson.mit-license.org/).
