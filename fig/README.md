### Figures for our [slides](../slides_20-01-2016) and [report](../report_final)
- Some are from Liu Pengkun's master thesis, but were not used,
- Some are from Wikimedia (CC licensed),
- Some are from other sources: <http://neverstopbuilding.com/minimax>, <http://beej.us/blog/data/minimax/>.
